# Dynamic DynamoDB - Slack Integration

## Setup

Setup a new **Slack incoming webhook** and select a channel where you want the Dynamic DynamoDB updates posted to.

https://my.slack.com/services/new/incoming-webhook/

Make a note of the **Webhook URL** (this is used by the Lambda function to post updates to Slack.)

Paste the webhook URL into the **config.json** file

## Dependencies

The package uses npm dependency called *request*. Make sure to install it using `npm install` before zipping the code for submitting.

## Configuration

The config variable in the `index.js` file can be used to adjust the username and bot icon in Slack.

## Lambda Setup

Lambda doesn't require any special IAM permissions (**Basic execution role** is enough.) Simply zip up the `index.js` and `node_modules` and upload to AWS Lambda with the following settings.

**Recommended runtime**: Node.js 4.3
**Handler**: `index.handler`

## SNS Setup

Notifications from Dynamic DynamoDB will be posted a topic in SNS. Create a new topic with a name of your choice.

Open the topic and create a new subscription.

**Protocol**: Lambda
**Endpoint**: ARN for the lambda function

## Dynamic DynamoDB Configuration

Make sure to set the **SNS Topic ARN** for the topic which will publish the updates and make sure to set the message types you are interested in. 

    sns-topic-arn: <sns_topic_arn_to_push_notifications_to>
    sns-message-types: scale-up, scale-down

The configuration file is usually located at (if installed from CloudFormation template)

	/etc/dynamic-dynamodb/dynamic-dynamodb.conf

The configuration options need to be set for all `[table]` and `[gsi]` sections that you want notified about

Once these changes are made, Dynamic DynamoDB needs to be restarted

`sudo service dynamic-dynamodb restart`