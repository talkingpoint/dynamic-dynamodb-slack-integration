var aws = require('aws-sdk');
var request = require("request");

// Read the config from the file
var config = {
	"webhookUrl": "<slack_webhook_url>",
	"botUsername": "DynamoDB Scaling Bot"
	//,"botIcon": "<bot_icon_url>"
}
 
exports.handler = function(event, context) {
	var record = event.Records[0].Sns;
	
	// Extract the subject and message from the notification
	var subject = record.Subject;
	var message = record.Message;
	
	// Compose a message to post to slack
	var output = {
		"username": config.botUsername,
		"icon_url": config.botIcon,
		//"text": subject + "\n\n" + message
		"text": message
	};
	
	// Post the message to slack
	request.post({ url: config.webhookUrl, method: 'POST', json: output },
		function (err, response, body) {
			if (err) {
				console.log(err);
				return context.fail(err);
			}

			return context.succeed('OK');
		}
	);
};